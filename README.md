# Test Django Project
## Start Project

```
    django-admin startproject TestDjango
```

---

## Create apps

```
    python3 manage.py startapp posts
```

## Create Super User

```
    python3 manage.py migrate
    python3 manage.py createsuperuser
```

## Other Relative Utilities

- django-daterangefilter
- django-import-export

```
    pip install django-daterangefilter
    pip install django-import-export
```