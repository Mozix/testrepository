# this file help us upload file to sample_data table 

from import_export import resources
from .models import sample_data

class DataResource(resources.ModelResource):

    class Meta:
        model = sample_data
