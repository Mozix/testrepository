from django.contrib import admin
from .models import sample_data, pdf_data
from daterangefilter.filters import PastDateRangeFilter
from import_export.admin import ImportExportModelAdmin

# Register your models here.

# Register sample_data
# Create SampleDataAdmin to customize data display, filter and search_fields
class SampleDataAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'name', 'update_date', 'buy_in_price', 'sold_out_price')
    search_fields = ('name', ) 
    list_filter = [
        ('update_date', PastDateRangeFilter)
    ]

admin.site.register(sample_data, SampleDataAdmin)

# Register pdf_data
# Create PDFAdmin to customize data display
# Note that pdf is not store at database
class PDFAdmin(admin.ModelAdmin):
    list_display = ('name', 'pdf', 'updated_on')

admin.site.register(pdf_data, PDFAdmin)

