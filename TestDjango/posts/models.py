from django.db import models
from django.utils import timezone
import datetime

# Create your models here.

# sample data model
# the following data define the sample_data table
class sample_data(models.Model):
    name = models.CharField(max_length=100)
    update_date = models.DateField()
    buy_in_price = models.IntegerField()
    sold_out_price = models.IntegerField()

    def __str__(self):
        return self.name

# pdf_data model
# the following data define the pdf_data table
# note that pdf is not save at database
class pdf_data(models.Model):
    name = models.CharField(max_length=25)
    pdf = models.FileField(blank=True)
    updated_on = models.DateTimeField(auto_now_add=False, auto_now=True)


    def __str__(self):
        return self.name
